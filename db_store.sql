-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 31, 2023 at 05:13 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_store`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `IdBarang` int(11) NOT NULL,
  `NamaBarang` varchar(50) DEFAULT NULL,
  `Keterangan` text DEFAULT NULL,
  `Satuan` varchar(50) DEFAULT NULL,
  `IdPengguna` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`IdBarang`, `NamaBarang`, `Keterangan`, `Satuan`, `IdPengguna`) VALUES
(1, 'Laptop', 'Laptop ASUS 8 GB', 'Unit', 14),
(2, 'Keyboard', 'Keyboard mekanik', 'Unit', 15),
(3, 'Mouse', 'Mouse optik wireless', 'Unit', 17),
(4, 'Monitor', 'Monitor LG 14 inch', 'Unit', 16),
(5, 'Printer', 'Printer all in one', 'Unit', 14),
(6, 'Speaker', 'Speaker bluetooth portable', 'Unit', 15),
(7, 'Smartphone', 'Smartphone OS android 13', 'Unit', 16),
(8, 'Flashdisk', 'Flashdisk USB 3.0 16 GB', 'Unit', 14),
(10, 'Kamera', 'Kamera Sony Mirrorless APS', 'Unit', 17),
(12, 'Earphone', 'Earphone Hitam', 'Unit', 15),
(13, 'Scanner', 'Scanner 5 in 1', 'Unit', 16);

-- --------------------------------------------------------

--
-- Table structure for table `hakakses`
--

CREATE TABLE `hakakses` (
  `IdAkses` int(11) NOT NULL,
  `NamaAkses` varchar(50) DEFAULT NULL,
  `Keterangan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hakakses`
--

INSERT INTO `hakakses` (`IdAkses`, `NamaAkses`, `Keterangan`) VALUES
(1, 'Admin', 'Mengelola aplikasi dan memiliki akses penuh'),
(2, 'Owner', 'Pemilik toko'),
(3, 'Manager', 'Pengelola dan management pengguna'),
(4, 'Finance', 'Mengelola keuangan toko'),
(5, 'Inventory', 'Mengelola persediaan atau gudang'),
(6, 'Marketing', 'Mengelola pemasaran dan penjualan'),
(7, 'Supplier', 'Pemasok barang atau kebutuhan toko'),
(9, 'Karyawan', 'Pekerja toko'),
(10, 'Customer', 'Pelanggan toko'),
(14, 'Super Admin', 'Memiliki hak akses penuh aplikasi');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `IdPelanggan` int(11) NOT NULL,
  `NamaPelanggan` varchar(50) NOT NULL,
  `NamaDepan` varchar(25) DEFAULT NULL,
  `NamaBelakang` varchar(25) DEFAULT NULL,
  `NoHP` varchar(15) DEFAULT NULL,
  `Alamat` varchar(100) DEFAULT NULL,
  `IdAkses` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`IdPelanggan`, `NamaPelanggan`, `NamaDepan`, `NamaBelakang`, `NoHP`, `Alamat`, `IdAkses`) VALUES
(1, 'Tya', 'Tya', 'Rawfah', '0821345671', 'Medan', 10),
(2, 'riskasye', 'Riska', 'Syelendra', '081234562112', 'Jl. Kususma Raya No.24, Jakarta Timur', 10),
(3, 'arkash69', 'Arkash', 'Romi', '082145656456', 'Jl. Kemang Raya No. 46, Jakarta Selatan', 10),
(4, 'bungalekky', 'Bunga', 'Anlekky', '085357654564', 'Jl. Adinugroho Gg.V No.17, Bekasi', 10),
(5, 'riantiutami', 'Rianti', 'Utami', '081233454545', 'Jl. Sudirman Kav No.20, Depok', 10),
(6, 'ardian_ryo', 'Ardian', 'Ryoto', '089534354534', 'Jl. Raya Bogor No.12, Kota Bogor', 10),
(7, 'robianto23', 'Robi', 'Santoso', '081235436655', 'Jl. Padat Karya,  Saigon, Pontianak Timur', 10),
(8, 'yumnair', 'Yumna', 'Airlangga', '082146546456', 'Jl. HM Suwignyo No.14,  Sungai Jawi, Bekasi', 10),
(9, 'syam_xair', 'Syam', 'Axair', '087822343543', 'Gang Usman Gani No.24, Bogor', 10),
(10, 'armolopo2', 'Armo', 'Delopo', '085333453534', 'Jl. Parit Leban,  Punggur Kecil, Pontianak Tenggara', 10),
(11, 'zeyn99zey', 'Zeyn', 'Malik', '085243666456', 'Jl. Anggrek Lestari Lebak Bulus, Jakarta Selatan', 10),
(12, 'gastiPratiwi', 'Gasti', 'Pratiwi', '085243661232', 'Jl Bina Jaya No.7,  Kota Baru, Pontianak Selatan', 10),
(13, 'kitalukita', 'Lukita', 'Napitupulu', '081245982325', 'Jalan Mega Jaya No. 12, Kabupaten Bogor', 10),
(14, 'mainah2098', 'Mainah', 'Sirait', '081309258834', 'Jl. Yam Sabran No.96, Tanggerang Selatan', 10),
(15, 'margana_prata', 'Margana', 'Prasetya', '081309345622', 'Gg. Pala 2 No.7, Tanggerang Selatan', 10),
(16, 'yunita', 'Yunita', 'Laras', '081309129833', 'Jl. Dharma Putra No.14, Depok', 10),
(17, 'maherohan', 'Mahesa', 'Tarihoran', '081309129833', 'Jl. Alianyang No.12A, Medan', 10),
(18, 'nadyarhm', 'Nadya', 'Rahma', '089534992345', 'Jl. Moh.Hatta No.14, Padang', 10),
(19, 'natsir_', 'Natsir', '', '08524356279532', 'Jl. Okta Mulya No.13, Pontiaank Timur', 10),
(20, 'restuyar', 'Restu', 'Yuniar', '082309458832', 'Jl. Moh.Hatta No.34, Padang', 10),
(21, 'gaiman_yat', 'Gaiman', 'Hidayat', '089512987344', 'Jl. Petani Komplek Abel Indah No 12, Bekasi', 10),
(23, 'Romi', 'Romeo', 'Ratuliu', '08533345352', 'Jakarta', 10),
(29, 'Tya', 'Tya', 'Axairo', '0821345671', 'Jakarta', 10);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `IdPembelian` int(11) NOT NULL,
  `JumlahPembelian` int(11) DEFAULT NULL,
  `HargaBeli` double DEFAULT NULL,
  `IdPengguna` int(11) DEFAULT NULL,
  `IdBarang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pembelian`
--

INSERT INTO `pembelian` (`IdPembelian`, `JumlahPembelian`, `HargaBeli`, `IdPengguna`, `IdBarang`) VALUES
(1, 30, 360000000, 15, 6),
(2, 50, 15000000, 16, 4),
(3, 200, 16000000, 14, 5),
(4, 10, 8000000, 15, 13),
(5, 150, 105000000, 15, 2),
(6, 70, 3500000, 16, 7),
(7, 20, 100000000, 15, 12),
(8, 50, 5000000, 14, 5),
(9, 30, 1500000, 17, 10),
(10, 17, 42500000, 15, 6),
(11, 40, 480000000, 14, 1),
(12, 85, 25500000, 15, 2),
(13, 35, 2800000, 14, 5),
(14, 10, 3500000, 15, 12),
(15, 15, 10500000, 14, 8),
(16, 130, 13000000, 17, 3),
(17, 20, 100000000, 15, 2),
(18, 60, 6000000, 16, 7),
(19, 80, 4000000, 16, 4),
(20, 35, 87500000, 14, 1),
(22, 100, 80000000, 14, 5),
(23, 10, 5000000, 16, 7),
(24, 26, 312000000, 14, 1),
(25, 200, 160000000, 16, 4);

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `IdPengguna` int(11) NOT NULL,
  `NamaPengguna` varchar(50) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `NamaDepan` varchar(50) DEFAULT NULL,
  `NamaBelakang` varchar(50) DEFAULT NULL,
  `NoHP` varchar(15) DEFAULT NULL,
  `Alamat` text DEFAULT NULL,
  `IdAkses` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`IdPengguna`, `NamaPengguna`, `Password`, `NamaDepan`, `NamaBelakang`, `NoHP`, `Alamat`, `IdAkses`) VALUES
(1, 'riskasye', '0860814ac6961f66fd9e33627802426861c671e9db5eef641840a6f1ec913920', 'Riska', 'Syelendra', '081234562112', 'Jakarta', 10),
(2, 'arkash69', 'a78b588fe2b1784bcbae2d3a38db5ccd45751c81a3b799f75291f397be63e6d6', 'Arkash', 'Romi', '082145656456', 'Jakarta', 10),
(3, 'bungaley', '03dd61616167dc4da7834efa525da15b', 'Bunga', 'Anlekky', '08535765452', 'Bekasi', 10),
(4, 'riantiutami', '3f5fb323295a4f844a668d5faee050659591b5d8ee581bc24d39b2eebf293982', 'Rianti', 'Utami', '081233454545', 'Depok', 10),
(5, 'ardian_ryo', 'a056cc8454330f198df145985515695c77ce188a85cb984f24c841e2641e6064', 'Ardian', 'Ryoto', '089534354534', 'Bogor', 10),
(6, 'robianto23', '498d1be5e24f1586afe8ff717bda90f92c1392d9a504d6c0ea6a0627f4e1cc56', 'Robi', 'Santoso', '081235436655', 'Jakarta', 10),
(7, 'yumnair', '2c32cecacf35135677efca833461c2a4e6cbff71cb636a89cdb833cf39948f54', 'Yumna', 'Airlangga', '082146546456', 'Bekasi', 10),
(8, 'syam_xair', '2aa0aa97992c46df239e8b9fc25e7174688714ef03129d45275dfd3e62e0172a', 'Syam', 'Axair', '087822343543', 'Bogor', 10),
(9, 'armolopo2', '170a012fcebd858b8c47b70878335d37714b971a3ed25faff8d06c81edca65f2', 'Armo', 'Delopo', '085333453534', 'Depok', 10),
(11, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin', NULL, '082134567823', 'Jakarta', 1),
(12, 'superadmin', '827ccb0eea8a706c4c34a16891f84e7b', 'Super', 'Admin', '0821345673', 'admin', 14),
(13, 'ardan', '1da5f689d169e9a8ccde10841c9759b2', 'Ardan', 'Ali', '0821345671', 'Jakarta', 2),
(14, 'jav_adc', '25d55ad283aa400af464c76d713c07ad', 'Jav', 'Adc', '081298476853', 'Cengkareng', 7),
(15, 'alumindo', '25d55ad283aa400af464c76d713c07ad', 'Alumindo', 'Industri', '085287127633', 'Pontianak ', 7),
(16, 'kyoei_work', '25d55ad283aa400af464c76d713c07ad', 'Kyoei', 'Work', '081312987615', 'Pontianak', 7),
(17, 'hastaPersada', '25d55ad283aa400af464c76d713c07ad', 'Hasta', 'Persada', '081312987612', 'Jakarta', 7);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `IdPenjualan` int(11) NOT NULL,
  `JumlahPenjualan` int(11) DEFAULT NULL,
  `HargaJual` double DEFAULT NULL,
  `IdPengguna` int(11) DEFAULT NULL,
  `IdBarang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`IdPenjualan`, `JumlahPenjualan`, `HargaJual`, `IdPengguna`, `IdBarang`) VALUES
(1, 30, 390000000, 6, 1),
(2, 50, 17500000, 7, 2),
(3, 115, 92000000, 4, 4),
(4, 98, 38220000, 8, 3),
(6, 70, 11900000, 6, 5),
(7, 18, 95400000, 7, 1),
(8, 50, 6750000, 4, 2),
(9, 30, 82500000, 6, 4),
(10, 15, 37500000, 8, 7),
(11, 30, 390000000, 6, 6),
(12, 80, 28000000, 7, 1),
(13, 30, 150000000, 4, 5),
(14, 10, 5000000, 8, 5),
(15, 13, 9750000, 4, 12),
(16, 5, 850000, 6, 6),
(17, 15, 79500000, 7, 8),
(18, 60, 54000000, 4, 5),
(19, 25, 75000000, 6, 10),
(20, 30, 82500000, 8, 3),
(22, 5, 25000000, 9, 2),
(23, 5, 350000, 6, 8),
(24, 5, 4000000, 2, 13);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `IdSupplier` int(11) NOT NULL,
  `NamaSupplier` varchar(50) NOT NULL,
  `Email` varchar(25) DEFAULT NULL,
  `NoHP` varchar(15) DEFAULT NULL,
  `Alamat` varchar(100) DEFAULT NULL,
  `IdAkses` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`IdSupplier`, `NamaSupplier`, `Email`, `NoHP`, `Alamat`, `IdAkses`) VALUES
(1, 'PT Intiksha', 'intiksha@gmail.com', '08213456743', 'Jakarta', 7),
(2, 'PT. Tembong Son Digdaya', 'tembongson@gmail.com', '081298346653', 'Jl. Matraman Raya No. 67, Jakarta Timur', 7),
(3, 'PT. Niaga Global Internusa', 'niagaglobal@gmail.com', '081209879934', 'LTC Glodok Lt. UG Blok A16 No. 2, Jakarta Barat', 7),
(4, 'CV. Jaya Bersama', 'jayabersama@gmail.com', '085388348755', 'Jl. Puri Anjasmoro Blok L - 12, Semarang', 7),
(5, 'CV. Luvindo Abadi', 'info@luvindo.co.id', '081298457832', 'Jl. Sersan Darmawangsa No.02, Jambi', 7),
(6, 'PD. Wahana Mandiri', 'wahanamandiri@gmail.com', '081298454366', 'Jl. Hayam Wuruk No.23, Jakarta Barat', 7),
(7, 'Jac Adv', 'jacadv@gmai.com', '081298476853', 'Ruko Pelangi Blok K No 2, Cengkareng', 7),
(8, 'PT. Triglobal Mandiri Indonesia', 'halotriglobal @trigobal-m', '081298471290', 'Kawasan Bisnis ITC BSD, Tanggerang Selatan', 7),
(9, 'PT. Alumindo Industri', 'alumindo@gmail.com', '085287127633', 'Jl. Pak Benceng No.C7, Pontianak Selatan', 7),
(10, 'PT. Siwani Makmur', 'temusiwani@siwani.co.id', '085345782381', 'Jl. Perdana No.23A, Depok', 7),
(11, 'PT. Multi Prima', 'mutliprima@gmail.com', '081312987612', 'Kawasan Bisnis ITC BSD Blok L, Tanggerang Selatan', 7),
(12, 'PT. Kyoei Work', 'kyoeiwork@kyoei.co.id', '081312987615', 'Jl. Parit Demang Dalam, Pontianak TImur', 7),
(13, 'PT. Hasta Persada', 'info@hasta-persada.co.id', '081312987612', 'Jl. Radio Dalam No.3, Jakarta Selatan', 7),
(14, 'PT. Panasia Indosyntec', 'panasia@indosyntec.co.id', '081312987612', 'Jl. Thamrin No.46B, Jakarta Pusat', 7),
(15, 'PT. Asri Industri', 'asriindustri@gmail.com', '082145656451', 'Jl. Tan Malaka No.14, Jakarta Utara', 7),
(16, 'PT. Intiprama', 'intiprama@gmail.com', '082145656413', 'Jl. Cilandak Raya No.15, Jakarta Selatan', 7),
(17, 'PT. Feedmil', 'inifeedmil@feedmil.co.id', '085357654731', 'Jl. Jendral Sudirman No.11, Jakarta Pusat', 7),
(18, 'PT. Sunson Pari', 'sunson@sunson-pari.co.id', '081312987712', 'Jl. Tabrani Ahmad No.17c, Bekasi', 7),
(19, 'PT. Tri Tirta', 'tritirta@gmail.com', '085333453522', 'Jl. Asia Jaya No.99a, Jakarta Pusat', 7),
(20, 'PT. Kemasindo', 'kemasindo@gmail.com', '082145656459', 'Jl. Thamrin No.21, Jakarta Pusat', 7),
(21, 'PT. Kreasi Sinar', 'infokreasi@kreasi-sinar.c', '085233453503', 'Jl. Jaya Baru No.22a, Tanggerang Selatan', 7),
(22, 'PT Jakaraya Utama', 'halo@jakaraya.co.id', '0821345622', 'Semarang', 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`IdBarang`),
  ADD KEY `IdPengguna` (`IdPengguna`);

--
-- Indexes for table `hakakses`
--
ALTER TABLE `hakakses`
  ADD PRIMARY KEY (`IdAkses`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`IdPelanggan`),
  ADD KEY `IdAkses` (`IdAkses`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`IdPembelian`),
  ADD KEY `IdPengguna` (`IdPengguna`),
  ADD KEY `IdBarang` (`IdBarang`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`IdPengguna`),
  ADD KEY `IdAkses` (`IdAkses`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`IdPenjualan`),
  ADD KEY `IdPengguna` (`IdPengguna`),
  ADD KEY `IdBarang` (`IdBarang`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`IdSupplier`),
  ADD KEY `IdAkses` (`IdAkses`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `IdBarang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `hakakses`
--
ALTER TABLE `hakakses`
  MODIFY `IdAkses` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `IdPelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `IdPembelian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `IdPengguna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `IdPenjualan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `IdSupplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`IdPengguna`) REFERENCES `pengguna` (`IdPengguna`);

--
-- Constraints for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD CONSTRAINT `pelanggan_ibfk_1` FOREIGN KEY (`idAkses`) REFERENCES `hakakses` (`IdAkses`),
  ADD CONSTRAINT `pelanggan_ibfk_2` FOREIGN KEY (`IdAkses`) REFERENCES `hakakses` (`IdAkses`);

--
-- Constraints for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD CONSTRAINT `pembelian_ibfk_1` FOREIGN KEY (`IdPengguna`) REFERENCES `pengguna` (`IdPengguna`),
  ADD CONSTRAINT `pembelian_ibfk_2` FOREIGN KEY (`IdBarang`) REFERENCES `barang` (`IdBarang`);

--
-- Constraints for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD CONSTRAINT `pengguna_ibfk_1` FOREIGN KEY (`IdAkses`) REFERENCES `hakakses` (`IdAkses`);

--
-- Constraints for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD CONSTRAINT `penjualan_ibfk_1` FOREIGN KEY (`IdPengguna`) REFERENCES `pengguna` (`IdPengguna`),
  ADD CONSTRAINT `penjualan_ibfk_2` FOREIGN KEY (`IdBarang`) REFERENCES `barang` (`IdBarang`);

--
-- Constraints for table `supplier`
--
ALTER TABLE `supplier`
  ADD CONSTRAINT `supplier_ibfk_1` FOREIGN KEY (`idAkses`) REFERENCES `hakakses` (`IdAkses`),
  ADD CONSTRAINT `supplier_ibfk_2` FOREIGN KEY (`IdAkses`) REFERENCES `hakakses` (`IdAkses`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
