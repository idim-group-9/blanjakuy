<a href="<?= base_url; ?>/penjualan" class="btn btn-primary mb-3"><i class="fa fa-angle-left"></i> Back </a>

<h4></br><b><?= $data['title'] ?></b></h4>

<div class="card card-body">
    <div class="table-responsive">
        <table class="table table-striped">
            <form role="form" action="<?= base_url; ?>/penjualan/updatePenjualan" method="POST" enctype="multipart/form-data">
                <tr>
                    <td>ID Penjualan</td>
                    <td><input type="text" class="form-control" name="IdPenjualan" value="<?= $data['penjualan']['IdPenjualan'] ?>" ; readonly></td>
                </tr>
                <tr>
                    <td>Nama Barang</td>
                    <td>
						<select name="IdBarang" class="form-control">
							<option value="#">Pilih Barang</option>
							<?php foreach ($data['barang'] as $row) : ?>
								<option value="<?= $row['IdBarang']; ?>" <?php if ($data['penjualan']['IdBarang'] == $row['IdBarang']) {
									echo "selected"; } ?>><?= $row['NamaBarang']; ?></option>
							<?php endforeach; ?>
						</select>
					</td>
                </tr>
                <tr>
					<td>Jumlah Penjualan</td>
					<td><input type="text" class="form-control" placeholder="Masukkan Jumlah Penjualan.." name="JumlahPenjualan" value="<?= $data['penjualan']['JumlahPenjualan'] ?>" ;></td>
				</tr>
                <tr>
					<td>Harga Jual</td>
					<td><input type="text" class="form-control" placeholder="Masukkan Harga Jual.." name="HargaJual" value="<?= $data['penjualan']['HargaJual'] ?>" ;></td>
				</tr>
                <tr>
                    <td>Nama Pengguna</td>
                    <td>
						<select name="IdPengguna" class="form-control">
							<option value="#">Pilih Pengguna</option>
							<?php foreach ($data['pengguna'] as $row) : ?>
								<option value="<?= $row['IdPengguna']; ?>" <?php if ($data['penjualan']['IdPengguna'] == $row['IdPengguna']) {
									echo "selected"; } ?>><?= $row['NamaPengguna']; ?></option>
							<?php endforeach; ?>
						</select>
					</td>
                </tr>
                <tr>
                    <td></td>
                    <td><button class="btn btn-primary"><i class="fa fa-edit"></i>Update Data</button></td>
                </tr>
            </form>
        </table>
    </div>
</div>