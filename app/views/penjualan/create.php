<a href="<?= base_url; ?>/penjualan" class="btn btn-primary mb-3"><i class="fa fa-angle-left"></i> Back </a> 
 <br/>

 <h4></br><b><?= $data['title'] ?></b></h4>
 
  <div class="row">
    <div class="col-sm-12">
      <?php
        Flasher::Message();
      ?>
    </div>
  </div>
      
<div class="card card-body">
	<div class="table-responsive">
		<table class="table table-striped">
			<form role="form" action="<?= base_url; ?>/penjualan/simpanPenjualan" method="POST" enctype="multipart/form-data">
                <tr>
					<td>Nama Barang</td>
                    <td>
						<select name="IdBarang" class="form-control">
							<option value="#">Pilih Barang</option>
							    <?php foreach ($data['barang'] as $row) :?>
                            <option value="<?= $row['IdBarang']; ?>"><?= $row['NamaBarang']; ?></option>
                        <?php endforeach; ?>
						</select>
					</td>				
                </tr>
                <tr>
					<td>Jumlah Penjualan</td>
					<td><input type="number" class="form-control" placeholder="Masukkan Jumlah Penjualan.." name="JumlahPenjualan" required></td>
				</tr>
                <tr>
					<td>Harga Jual</td>
					<td><input type="number" class="form-control" placeholder="Masukkan Harga Jual.." name="HargaJual" required></td>
				</tr>
                <tr>
					<td>Id Pengguna</td>
                    <td>
						<select name="IdPengguna" class="form-control">
							<option value="#">Pilih Pengguna</option>
							    <?php foreach ($data['pengguna'] as $row) :?>
                            <option value="<?= $row['IdPengguna']; ?>"><?= $row['NamaPengguna']; ?></option>
                        <?php endforeach; ?>
						</select>
					</td>				
                </tr>
				<tr>
					<td></td>
					<td><button class="btn btn-primary"><i class="fa fa-edit"></i> Insert Data</button></td>
				</tr>
			</form>
		</table>
	</div>
</div>
