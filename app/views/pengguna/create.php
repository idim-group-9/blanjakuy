<a href="<?= base_url; ?>/pengguna" class="btn btn-primary mb-3"><i class="fa fa-angle-left"></i> Back </a>

<h4></br> <b><?= $data['title'] ?></b></h4>

<div class="row">
	<div class="col-sm-12">
		<?php
		Flasher::Message();
		?>
	</div>
</div>

<div class="card card-body">
	<div class="table-responsive">
		<table class="table table-striped">
			<form role="form" action="<?= base_url; ?>/pengguna/simpanPengguna" method="POST" enctype="multipart/form-data">
				<tr>
					<td>Nama Pengguna</td>
					<td><input type="text" class="form-control" placeholder="Masukkan Nama Pengguna.." name="NamaPengguna" required></td>
				</tr>
				<tr>
					<td>Nama Depan</td>
					<td><input type="text" class="form-control" placeholder="Masukkan Nama Depan.." name="NamaDepan" required></td>
				</tr>
				<tr>
					<td>Nama Belakang</td>
					<td><input type="text" class="form-control" placeholder="Masukkan Nama Belakang.." name="NamaBelakang" required></td>
				</tr>
				<tr>
					<td>No Hp</td>
					<td><input type="text" class="form-control" placeholder="Masukkan No Hp.." name="NoHP" required></td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td><input type="text" class="form-control" placeholder="Masukkan Nama Alamat.." name="Alamat" required></td>
				</tr>
				<tr>
					<td>Id Akses</td>
					<td>
						<select name="IdAkses" class="form-control">
							<option value="#">Pilih Hak Akses</option>
							<?php foreach ($data['role'] as $row) : ?>
								<option value="<?= $row['IdAkses']; ?>"><?= $row['NamaAkses']; ?></option>
							<?php endforeach; ?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Password</td>
					<td><input type="password" class="form-control" placeholder="Masukkan Password.." name="Password" required></td>
				</tr>
				<tr>
					<td>Ulangi Password</td>
					<td><input type="password" class="form-control" name="ulangi_password" required></td>
				</tr>
				<tr>
					<td></td>
					<td><button class="btn btn-primary"><i class="fa fa-edit"></i> Insert Data</button></td>
				</tr>
			</form>
		</table>
	</div>
</div>