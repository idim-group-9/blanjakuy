<a href="<?= base_url; ?>/pengguna" class="btn btn-primary mb-3"><i class="fa fa-angle-left"></i> Back </a>

<h4></br><b><?= $data['title'] ?></b></h4>

<div class="card card-body">
	<div class="table-responsive">
		<table class="table table-striped">
			<form role="form" action="<?= base_url; ?>/pengguna/updatePengguna" method="POST" enctype="multipart/form-data">
				<tr>
					<td>ID Pengguna</td>
					<td><input type="text" class="form-control" name="IdPengguna" value="<?= $data['pengguna']['IdPengguna'] ?>" ; readonly></td>
				</tr>
				<tr>
					<td>Nama Pengguna</td>
					<td><input type="text" class="form-control" placeholder="Masukkan Nama Pengguna.." name="NamaPengguna" value="<?= $data['pengguna']['NamaPengguna'] ?>" ;></td>
				</tr>
				<tr>
					<td>Nama Depan</td>
					<td><input type="text" class="form-control" placeholder="Masukkan Nama Depan.." name="NamaDepan" value="<?= $data['pengguna']['NamaDepan'] ?>" ;></td>
				</tr>
				<tr>
					<td>Nama Belakang</td>
					<td><input type="text" class="form-control" placeholder="Masukkan Nama Belakang.." name="NamaBelakang" value="<?= $data['pengguna']['NamaBelakang'] ?>" ;></td>
				</tr>
				<tr>
					<td>No Hp</td>
					<td><input type="text" class="form-control" placeholder="Masukkan No Hp.." name="NoHP" value="<?= $data['pengguna']['NoHP'] ?>" ;></td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td><input type="text" class="form-control" placeholder="Masukkan Nama Alamat.." name="Alamat" value="<?= $data['pengguna']['Alamat'] ?>" ;></td>
				</tr>
				<tr>
					<td>Hak Akses</td>
					<td>
						<select name="IdAkses" class="form-control">
							<option value="#">Pilih Hak Akses</option>
							<?php foreach ($data['role'] as $row) : ?>
								<option value="<?= $row['IdAkses']; ?>" <?php if ($data['pengguna']['IdAkses'] == $row['IdAkses']) {
									echo "selected"; } ?>><?= $row['NamaAkses']; ?></option>
							<?php endforeach; ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><b>Ganti Password<b></td>
					<td></td>
				</tr>
				<tr>
					<td>Password Baru</td>
					<td><input type="password" class="form-control" placeholder="Masukkan Password Baru.." name="Password"></td>
				</tr>
				<tr>
					<td>Ulangi Password Baru</td>
					<td><input type="password" class="form-control" name="ulangi_password"></td>
				</tr>
				<tr>
					<td></td>
					<td><button class="btn btn-primary"><i class="fa fa-edit"></i>Update Data</button></td>
				</tr>
			</form>
		</table>
	</div>
</div>