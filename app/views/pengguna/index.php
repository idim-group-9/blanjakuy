<h4><b><?= $data['title'] ?></b></h4>
<br />

<div class="row">
    <div class="col-sm-12">
        <?php
        Flasher::Message();
        ?>
    </div>
</div>

<!-- Trigger the modal with a button -->
<a href="<?= base_url; ?>/pengguna/tambah">
    <button type="button" class="btn btn-primary btn-md mr-2" data-toggle="modal" data-target="#myModal">
        <i class="fa fa-plus"></i> Insert Data</button>
</a>
<div class="clearfix"></div>
<br />
<!-- view barang -->
<div class="card card-body">
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-sm" id="example1">
            <thead>
                <tr style="background:#DFF0D8;color:#333;" align="center">
                    <th>No.</th>
                    <th>ID Pengguna</th>
                    <th>Nama Pengguna</th>
                    <th>Nama Depan</th>
                    <th>Nama Belakang</th>
                    <th>No Hp</th>
                    <th>Alamat</th>
                    <th>ID Akses</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <?php
                    foreach ($data['pengguna'] as $key => $data) {
                    ?>
                        <td> <?= $key + 1 ?> </td>
                        <td> <?= $data["IdPengguna"] ?> </td>
                        <td> <?= $data["NamaPengguna"] ?> </td>
                        <td> <?= $data["NamaDepan"] ?> </td>
                        <td> <?= $data["NamaBelakang"] ?> </td>
                        <td> <?= $data["NoHP"] ?> </td>
                        <td> <?= $data["Alamat"] ?> </td>
                        <td> <?= $data["IdAkses"] ?> </td>

                        <td align="center">
                            <a href="<?= base_url; ?>/pengguna/edit/<?= $data['IdPengguna'] ?>">
                                <button class="btn btn-warning btn-xs">Edit</button></a>

                            <a href="<?= base_url; ?>/pengguna/hapus/<?= $data['IdPengguna'] ?>" onclick="return confirm('Hapus data?');">
                                <button class="btn btn-danger btn-xs">Hapus</button></a>

                        </td>
                </tr>
            <?php
                    }
            ?>
            </tbody>
        </table>
    </div>
</div>