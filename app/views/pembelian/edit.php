<a href="<?= base_url; ?>/pembelian" class="btn btn-primary mb-3"><i class="fa fa-angle-left"></i> Back </a>

<h4></br><b><?= $data['title'] ?></b></h4>

<div class="card card-body">
    <div class="table-responsive">
        <table class="table table-striped">
            <form role="form" action="<?= base_url; ?>/pembelian/updatePembelian" method="POST" enctype="multipart/form-data">
                <tr>
                    <td>ID Pembelian</td>
                    <td><input type="text" class="form-control" name="IdPembelian" value="<?= $data['pembelian']['IdPembelian'] ?>" ; readonly></td>
                </tr>
                <tr>
                    <td>Nama Barang</td>
                    <td>
						<select name="IdBarang" class="form-control">
							<option value="#">Pilih Barang</option>
							<?php foreach ($data['barang'] as $row) : ?>
								<option value="<?= $row['IdBarang']; ?>" <?php if ($data['pembelian']['IdBarang'] == $row['IdBarang']) {
									echo "selected"; } ?>><?= $row['NamaBarang']; ?></option>
							<?php endforeach; ?>
						</select>
					</td>
                </tr>
                <tr>
					<td>Jumlah Pembelian</td>
					<td><input type="text" class="form-control" placeholder="Masukkan Jumlah Pembelian.." name="JumlahPembelian" value="<?= $data['pembelian']['JumlahPembelian'] ?>" ;></td>
				</tr>
                <tr>
					<td>Harga Beli</td>
					<td><input type="text" class="form-control" placeholder="Masukkan Harga Beli.." name="HargaBeli" value="<?= $data['pembelian']['HargaBeli'] ?>" ;></td>
				</tr>
                <tr>
                    <td>Nama Pengguna</td>
                    <td>
						<select name="IdPengguna" class="form-control">
							<option value="#">Pilih Pengguna</option>
							<?php foreach ($data['pengguna'] as $row) : ?>
								<option value="<?= $row['IdPengguna']; ?>" <?php if ($data['pembelian']['IdPengguna'] == $row['IdPengguna']) {
									echo "selected"; } ?>><?= $row['NamaPengguna']; ?></option>
							<?php endforeach; ?>
						</select>
					</td>
                </tr>
                <tr>
                    <td></td>
                    <td><button class="btn btn-primary"><i class="fa fa-edit"></i>Update Data</button></td>
                </tr>
            </form>
        </table>
    </div>
</div>