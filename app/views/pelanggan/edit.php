<a href="<?= base_url; ?>/pelanggan" class="btn btn-primary mb-3"><i class="fa fa-angle-left"></i> Back </a>

<h4></br><b><?= $data['title'] ?></b></h4>

<div class="card card-body">
	<div class="table-responsive">
		<table class="table table-striped">
			<form role="form" action="<?= base_url; ?>/pelanggan/updatePelanggan" method="POST" enctype="multipart/form-data">
				<tr>
					<td>ID Pelanggan</td>
					<td><input type="text" class="form-control" name="IdPelanggan" value="<?= $data['pelanggan']['IdPelanggan'] ?>" ; readonly></td>
				</tr>
				<tr>
					<td>Nama Pelanggan</td>
					<td><input type="text" class="form-control" placeholder="Masukkan Nama Pelanggan.." name="NamaPelanggan" value="<?= $data['pelanggan']['NamaPelanggan'] ?>" ;></td>
				</tr>
				<tr>
					<td>Nama Depan</td>
					<td><input type="text" class="form-control" placeholder="Masukkan Nama Depan.." name="NamaDepan" value="<?= $data['pelanggan']['NamaDepan'] ?>" ;></td>
				</tr>
				<tr>
					<td>Nama Belakang</td>
					<td><input type="text" class="form-control" placeholder="Masukkan Nama Belakang.." name="NamaBelakang" value="<?= $data['pelanggan']['NamaBelakang'] ?>" ;></td>
				</tr>
				<tr>
					<td>No Hp</td>
					<td><input type="text" class="form-control" placeholder="Masukkan No Hp.." name="NoHP" value="<?= $data['pelanggan']['NoHP'] ?>" ;></td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td><input type="text" class="form-control" placeholder="Masukkan Nama Alamat.." name="Alamat" value="<?= $data['pelanggan']['Alamat'] ?>" ;></td>
				</tr>
				<tr>
					<td></td>
					<td><button class="btn btn-primary"><i class="fa fa-edit"></i>Update Data</button></td>
				</tr>
			</form>
		</table>
	</div>
</div>