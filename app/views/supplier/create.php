<a href="<?= base_url; ?>/supplier" class="btn btn-primary mb-3"><i class="fa fa-angle-left"></i> Back </a>

<h4></br> <b><?= $data['title'] ?></b></h4>

<div class="row">
	<div class="col-sm-12">
		<?php
		Flasher::Message();
		?>
	</div>
</div>

<div class="card card-body">
	<div class="table-responsive">
		<table class="table table-striped">
			<form role="form" action="<?= base_url; ?>/supplier/simpanSupplier" method="POST" enctype="multipart/form-data">
				<tr>
					<td>Nama Supplier</td>
					<td><input type="text" class="form-control" placeholder="Masukkan Nama Supplier.." name="NamaSupplier" required></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><input type="email" class="form-control" placeholder="Masukkan Email.." name="Email" required></td>
				</tr>
				<tr>
					<td>No Hp</td>
					<td><input type="text" class="form-control" placeholder="Masukkan No Hp.." name="NoHP" required></td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td><input type="text" class="form-control" placeholder="Masukkan Nama Alamat.." name="Alamat" required></td>
				</tr>
                
				<tr>
					<td></td>
					<td><button class="btn btn-primary"><i class="fa fa-edit"></i> Insert Data</button></td>
				</tr>
			</form>
		</table>
	</div>
</div>