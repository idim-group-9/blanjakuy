<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<div class="row">
    <!-- STATUS cardS -->
    <div class="col-md-6 mb-6">
        <div class="card">
            <div class="card-header bg-primary text-white">
                <h6 class="pt-2"><i class="fas fa-upload"></i> Telah Terjual</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">

                    <div style="width: 100%; margin: auto;">
                        <canvas id="sellItems"></canvas>
                    </div>

                    <?php
                    $totalSell = array(128, 105, 128, 145, 170, 35, 15, 20, 25, 13, 5);
                    ?>

                    <script>
                        // Data laba rugi
                        var labels = ['Laptop', 'Keyboard', 'Mouse', 'Monitor', 'Printer', 'Speaker', 'Smartphone', 'Flashdisk', 'Kamera', 'Earphone', 'Scanner'];
                        var totalSell = <?php echo json_encode($totalSell); ?>;

                        // Buat grafik
                        var ctx = document.getElementById('sellItems').getContext('2d');
                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: {
                                labels: labels,
                                datasets: [{
                                    label: 'Total Terjual',
                                    data: totalSell,
                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(75, 192, 192, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(255, 159, 64, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)',
                                        'rgba(54, 162, 235, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(75, 192, 192, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(255, 159, 64, 1)'
                                    ],
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                responsive: true,
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                    </script>
                </div>
            </div>
        </div>
        <!--/grey-card -->
    </div><!-- /col-md-3-->

    <!--STATUS cardS -->
    <div class="col-md-6 mb-6">
        <div class="card">
            <div class="card-header bg-primary text-white">
                <h6 class="pt-2"><i class="fas fa-cubes"></i> Laba Rugi</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">

                    <div style="width: 100%; margin: auto;">
                        <canvas id="incomeStatementChart"></canvas>
                    </div>

                    <?php
                    $income = array(879500000, 49250000, 120720000, 174500000, 220900000, 390850000, 37500000, 79850000, 75000000, 19750000, 9000000); // Pendapatan
                    $expenses = array(513400000, 23050000, 13000000, 179000000, 103800000, 402500000, 14500000, 10500000, 1500000, 103500000, 8000000); // Biaya

                    // Menghitung laba (pendapatan - biaya)
                    $profit = array();
                    for ($i = 0; $i < count($income); $i++) {
                        $profit[$i] = $income[$i] - $expenses[$i];
                    }
                    ?>

                    <script>
                        // Data laba rugi
                        var labels = ['Laptop', 'Keyboard', 'Mouse', 'Monitor', 'Printer', 'Speaker', 'Smartphone', 'Flashdisk', 'Kamera', 'Earphone', 'Scanner'];
                        var incomeData = <?php echo json_encode($income); ?>;
                        var expensesData = <?php echo json_encode($expenses); ?>;
                        var profitData = <?php echo json_encode($profit); ?>;

                        // Buat grafik
                        var ctx = document.getElementById('incomeStatementChart').getContext('2d');
                        var myChart = new Chart(ctx, {
                            type: 'line',
                            data: {
                                labels: labels,
                                datasets: [{
                                    label: 'Pendapatan',
                                    data: incomeData,
                                    borderColor: 'green',
                                    fill: false,
                                }, {
                                    label: 'Biaya',
                                    data: expensesData,
                                    borderColor: 'red',
                                    fill: false,
                                }, {
                                    label: 'Laba',
                                    data: profitData,
                                    borderColor: 'blue',
                                    fill: false,
                                }]
                            },
                            options: {
                                responsive: true,
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                    </script>
                </div>
            </div>
        </div>
        <!--/grey-card -->
    </div><!-- /col-md-3-->
    <!-- STATUS cardS -->
</div>

</br>
<div class="row">
    <!--STATUS cardS -->
    <div class="col-md-6 mb-6">
        <div class="card">
            <div class="card-header bg-primary text-white">
                <h6 class="pt-2"><i class="fas fa-upload"></i> Stok Barang</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">

                    <div style="width: 100%; margin: auto;">
                        <canvas id="stockItem"></canvas>
                    </div>

                    <?php
                    $stock = array(27, 150, 2, 185, 170, 200, 32, 120, 10, 17, 5);
                    ?>

                    <script>
                        // Data laba rugi
                        var labels = ['Laptop', 'Keyboard', 'Mouse', 'Monitor', 'Printer', 'Speaker', 'Smartphone', 'Flashdisk', 'Kamera', 'Earphone', 'Scanner'];
                        var stock = <?php echo json_encode($stock); ?>;

                        // Buat grafik
                        var ctx = document.getElementById('stockItem').getContext('2d');
                        var myChart = new Chart(ctx, {
                            type: 'bar',
                            data: {
                                labels: labels,
                                datasets: [{
                                    label: 'Stok Barang',
                                    data: stock,
                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(75, 192, 192, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(255, 159, 64, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255,99,132,1)',
                                        'rgba(54, 162, 235, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(75, 192, 192, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(255, 159, 64, 1)'
                                    ],
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                responsive: true,
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                    </script>
                </div>
            </div>
        </div>
        <!--/grey-card -->
    </div><!-- /col-md-3-->

    <div class="col-md-6 mb-6">
        <div class="card">
            <div class="card-header bg-primary text-white">
                <h6 class="pt-2"><i class="fas fa-cubes"></i> Kombinasi Produk Terbaik</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <?php
                        $result = linearProgramming();
                    ?>
                    <table class="table table-bordered table-striped table-sm" id="linearProgramming">
                    <thead>
                        <tr style="background:#F0F8FF;color:#653;" align="center">
                            <th>No.</th>
                            <th>Nama Barang</th>
                            <th>Jumlah Jual</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> 1 </td>
                            <td> Laptop </td>
                            <td> <?= $result['optimalX'] ?> unit</td>
                        </tr>
                        <tr>
                            <td> 2 </td>
                            <td> Printer </td>
                            <td> <?= $result['optimalY'] ?> unit</td>
                        </tr>
                        <tr>
                            <td>  </td>
                            <td> <b> Keuntungan Maksimal </b></td>
                            <td><b> Rp. <?= $result['maxProfit'] ?> </b></td>
                        </tr>
                    </tbody>
                </table>        
                </div>
            </div>
        </div>
        <!--/grey-card -->
    </div><!-- /col-md-3-->
</div>

</br>
<?php
function objectiveFunction($x, $y)
{
    // Harga dan keuntungan per unit produk
    $priceX = 14000000;
    $profitX = 2000000;
    $priceY = 950000;
    $profitY = 200000;

    // Fungsi tujuan untuk memaksimalkan keuntungan
    return ($priceX * $x * $profitX) + ($priceY * $y * $profitY);
}

// Contoh fungsi kendala (ketersediaan stok dan waktu produksi)
function constraints($x, $y)
{
    // Ketersediaan stok produk
    $availableStockX = 27;
    $availableStockY = 215;

    // Waktu produksi yang tersedia
    $availableTime = 100;

    // Fungsi kendala
    return ($x <= $availableStockX) && ($y <= $availableStockY) && (($x + $y) <= $availableTime);
}

// Implementasi algoritma Simplex untuk mencari kombinasi produk optimal
function linearProgramming()
{
    $optimalX = 0;
    $optimalY = 0;
    $maxProfit = 0;

    for ($x = 0; $x <= 215; $x++) {
        for ($y = 0; $y <= 27; $y++) {
            if (constraints($x, $y)) {
                $profit = objectiveFunction($x, $y);
                if ($profit > $maxProfit) {
                    $maxProfit = $profit;
                    $optimalX = $x;
                    $optimalY = $y;
                }
            }
        }
    }

    return [
        'optimalX' => $optimalX,
        'optimalY' => $optimalY,
        'maxProfit' => $maxProfit,
    ];
}
