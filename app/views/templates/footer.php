  </div>
    <!-- /.container-fluid -->

  </div>

  <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>
                    <?php echo date('Y');?> - Sistem Penjualan Barang Berbasis Web |
                    BY GROUP 9</a></b>
                </span>
            </div>
        </div>
  </footer>

  </div>
  <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    
    <!-- Custom scripts for all pages-->
    <script src="<?= base_url; ?>/sbadmin/js/sb-admin-2.min.js"></script>
    <script src="<?= base_url; ?>/sbadmin/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url; ?>/sbadmin/vendor/datatables/dataTables.bootstrap4.min.js"></script>
</body>
</html>