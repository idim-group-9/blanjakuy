<a href="<?= base_url; ?>/role" class="btn btn-primary mb-3"><i class="fa fa-angle-left"></i> Back </a> 

<h4></br><b><?= $data['title'] ?></b></h4>

<div class="card card-body">
   <div class="table-responsive">
       <table class="table table-striped">
           <form role="form" action="<?= base_url; ?>/role/updateRole" method="POST" enctype="multipart/form-data">
               <tr>
                   <td>ID Akses</td>
                   <td><input type="text" class="form-control" name="IdAkses" value="<?= $data['role']['IdAkses'] ?>"; readonly></td>              
                </tr>
                <tr>
                   <td>Nama Akses</td>
                   <td><input type="text" class="form-control" placeholder="Masukkan Nama Hak Akses.." name="NamaAkses" value="<?= $data['role']['NamaAkses'] ?>";></td>
               </tr>
               <tr>
                   <td>Keterangan</td>
                   <td><input type="text" class="form-control" placeholder="Masukkan Keterangan.." name="Keterangan" value="<?= $data['role']['Keterangan'] ?>";></td>
               </tr>
               <tr>
                   <td></td>
                   <td><button class="btn btn-primary"><i class="fa fa-edit"></i> Update Data</button></td>
               </tr>
           </form>
       </table>
   </div>
</div>