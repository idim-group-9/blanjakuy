<?php

class PelangganModel
{

    private $table = 'pelanggan';
    private $db, $db2;

    public function __construct()
    {
        $this->db = new Database;
        $this->db2 = new Database;
    }

    public function getAllPelanggan()
    {
        $this->db->query('SELECT * FROM pelanggan ORDER BY IdPelanggan ASC');
        return $this->db->resultSet();
    }

    public function getPelangganById($id)
    {
        $this->db->query('SELECT * FROM ' . $this->table . ' WHERE IdPelanggan=:IdPelanggan');
        $this->db->bind('IdPelanggan', $id);
        return $this->db->single();
    }

    public function tambahPelanggan($data)
    {
        $this->db->query('SELECT * FROM hakakses WHERE NamaAkses=:NamaAkses');
        $this->db->bind('NamaAkses', 'customer');

        $idAkses = $this->db->single();
       
        $query = "INSERT INTO pelanggan (NamaPelanggan,NamaDepan,NamaBelakang,NoHP,Alamat,IdAkses) VALUES(:NamaPelanggan,:NamaDepan,:NamaBelakang,:NoHP,:Alamat,:IdAkses)";
        $this->db2->query($query);
        $this->db2->bind('NamaPelanggan', $data['NamaPelanggan']);
        $this->db2->bind('NamaDepan', $data['NamaDepan']);
        $this->db2->bind('NamaBelakang', $data['NamaBelakang']);
        $this->db2->bind('NoHP', $data['NoHP']);
        $this->db2->bind('Alamat', $data['Alamat']);
        $this->db2->bind('IdAkses', $idAkses['IdAkses']);
        $this->db2->execute();

        return $this->db2->rowCount();
    }

    public function updateDataPelanggan($data)
    {
        $query = "UPDATE pelanggan SET NamaPelanggan=:NamaPelanggan, NamaDepan=:NamaDepan, NamaBelakang=:NamaBelakang, NoHP=:NoHP, Alamat=:Alamat WHERE IdPelanggan=:IdPelanggan";
        $this->db->query($query);
        $this->db->bind('IdPelanggan', $data['IdPelanggan']);
        $this->db->bind('NamaPelanggan', $data['NamaPelanggan']);
        $this->db->bind('NamaDepan', $data['NamaDepan']);
        $this->db->bind('NamaBelakang', $data['NamaBelakang']);
        $this->db->bind('NoHP', $data['NoHP']);
        $this->db->bind('Alamat', $data['Alamat']);
        $this->db->execute();

        return $this->db->rowCount();
    }

    public function deletePelanggan($id)
    {
        $this->db->query('DELETE FROM ' . $this->table . ' WHERE IdPelanggan=:IdPelanggan');
        $this->db->bind('IdPelanggan', $id);
        $this->db->execute();

        return $this->db->rowCount();
    }
}
