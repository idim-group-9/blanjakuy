<?php

class SupplierModel
{

    private $table = 'supplier';
    private $db, $db2;

    public function __construct()
    {
        $this->db = new Database;
        $this->db2 = new Database;
    }

    public function getAllSupplier()
    {
        $this->db->query('SELECT * FROM supplier ORDER BY IdSupplier');
        return $this->db->resultSet();
    }

    public function getSupplierById($id)
    {
        $this->db->query('SELECT * FROM ' . $this->table . ' WHERE IdSupplier=:IdSupplier');
        $this->db->bind('IdSupplier', $id);
        return $this->db->single();
    }

    public function tambahSupplier($data)
    {
        $this->db->query('SELECT * FROM hakakses WHERE NamaAkses=:NamaAkses');
        $this->db->bind('NamaAkses', 'Supplier');

        $idAkses = $this->db->single();
       
        $query = "INSERT INTO supplier (NamaSupplier,Email,NoHP,Alamat,IdAkses) VALUES(:NamaSupplier,:Email,:NoHP,:Alamat,:IdAkses)";
        $this->db2->query($query);
        $this->db2->bind('NamaSupplier', $data['NamaSupplier']);
        $this->db2->bind('Email', $data['Email']);
        $this->db2->bind('NoHP', $data['NoHP']);
        $this->db2->bind('Alamat', $data['Alamat']);
        $this->db2->bind('IdAkses', $idAkses['IdAkses']);
        $this->db2->execute();

        return $this->db2->rowCount();
    }

    public function updateDataSupplier($data)
    {
        $query = "UPDATE supplier SET NamaSupplier=:NamaSupplier, Email=:Email, NoHP=:NoHP, Alamat=:Alamat WHERE IdSupplier=:IdSupplier";
        $this->db->query($query);
        $this->db->bind('IdSupplier', $data['IdSupplier']);
        $this->db->bind('NamaSupplier', $data['NamaSupplier']);
        $this->db->bind('Email', $data['Email']);
        $this->db->bind('NoHP', $data['NoHP']);
        $this->db->bind('Alamat', $data['Alamat']);
        $this->db->execute();

        return $this->db->rowCount();
    }

    public function deleteSupplier($id)
    {
        $this->db->query('DELETE FROM ' . $this->table . ' WHERE IdSupplier=:IdSupplier');
        $this->db->bind('IdSupplier', $id);
        $this->db->execute();

        return $this->db->rowCount();
    }
}
