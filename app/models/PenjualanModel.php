<?php

class PenjualanModel {
	
	private $table = 'penjualan';
	private $db;

	public function __construct()
	{
		$this->db = new Database;
	}

	public function getAllPenjualan()
	{
		$this->db->query('SELECT IdPenjualan, JumlahPenjualan, HargaJual, a.IdPengguna, b.NamaPengguna, a.IdBarang, c.NamaBarang FROM penjualan a JOIN pengguna b ON a.IdPengguna = b.IdPengguna JOIN barang c ON a.IdBarang = c.IdBarang ORDER BY a.IdPenjualan ASC');
		return $this->db->resultSet();
	}

	public function getPenjualanById($id)
	{
		$this->db->query('SELECT * FROM ' . $this->table . ' WHERE IdPenjualan=:IdPenjualan');
		$this->db->bind('IdPenjualan',$id);
		return $this->db->single();
	}

	public function tambahPenjualan($data)
	{
		$query = "INSERT INTO penjualan (JumlahPenjualan, HargaJual, IdPengguna, IdBarang) VALUES(:JumlahPenjualan, :HargaJual, :IdPengguna, :IdBarang)";
		$this->db->query($query);
		$this->db->bind('JumlahPenjualan',$data['JumlahPenjualan']);
		$this->db->bind('HargaJual',$data['HargaJual']);
        $this->db->bind('IdPengguna',$data['IdPengguna']);
        $this->db->bind('IdBarang',$data['IdBarang']);
		$this->db->execute();

		return $this->db->rowCount();
	}
	
	public function updateDataPenjualan($data)
	{
		$query = "UPDATE penjualan SET JumlahPenjualan=:JumlahPenjualan, HargaJual=:HargaJual, IdPengguna=:IdPengguna, IdBarang=:IdBarang WHERE IdPenjualan=:IdPenjualan";
		$this->db->query($query);
		$this->db->bind('IdPenjualan',$data['IdPenjualan']);
		$this->db->bind('JumlahPenjualan',$data['JumlahPenjualan']);
		$this->db->bind('HargaJual',$data['HargaJual']);
		$this->db->bind('IdPengguna',$data['IdPengguna']);
		$this->db->bind('IdBarang',$data['IdBarang']);
		$this->db->execute();

		return $this->db->rowCount();
	}

	public function deletePenjualan($id)
	{
		$this->db->query('DELETE FROM ' . $this->table . ' WHERE IdPenjualan=:IdPenjualan');
		$this->db->bind('IdPenjualan',$id);
		$this->db->execute();

		return $this->db->rowCount();
	}

	public function getJumlahPenjualan()
	{
		$this->db->query('SELECT SUM(HargaJual) AS HargaJual FROM penjualan GROUP BY penjualan.IdBarang');
		return $this->db->resultSet();
	}
}