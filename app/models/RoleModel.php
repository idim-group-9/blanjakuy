<?php

class RoleModel {
	
	private $table = 'hakakses';
	private $db;

	public function __construct()
	{
		$this->db = new Database;
	}

	public function getAllRole()
	{
		$this->db->query('SELECT * FROM hakakses ORDER BY IdAkses ASC');
		return $this->db->resultSet();
	}

	public function getRoleById($id)
	{
		$this->db->query('SELECT * FROM ' . $this->table . ' WHERE IdAkses=:IdAkses');
		$this->db->bind('IdAkses',$id);
		return $this->db->single();
	}

	public function tambahRole($data)
	{
		$query = "INSERT INTO hakakses (NamaAkses, Keterangan) VALUES(:NamaAkses, :Keterangan)";
		$this->db->query($query);
		$this->db->bind('NamaAkses',$data['NamaAkses']);
		$this->db->bind('Keterangan',$data['Keterangan']);

		$this->db->execute();

		return $this->db->rowCount();
	}
	
	public function updateDataRole($data)
	{
		$query = "UPDATE hakakses SET NamaAkses=:NamaAkses, Keterangan=:Keterangan WHERE IdAkses=:IdAkses";
		$this->db->query($query);
		$this->db->bind('IdAkses',$data['IdAkses']);
		$this->db->bind('NamaAkses',$data['NamaAkses']);
		$this->db->bind('Keterangan',$data['Keterangan']);
		$this->db->execute();

		return $this->db->rowCount();
	}

	public function deleteRole($id)
	{
		$this->db->query('DELETE FROM ' . $this->table . ' WHERE IdAkses=:IdAkses');
		$this->db->bind('IdAkses',$id);
		$this->db->execute();

		return $this->db->rowCount();
	}

	public function cariRole()
	{
		$key = $_POST['key'];
		$this->db->query("SELECT * FROM " . $this->table . " WHERE NamaAkses LIKE :key");
		$this->db->bind('key',"%$key%");
		return $this->db->resultSet();
	}
}