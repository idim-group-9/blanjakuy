<?php

class Pengguna extends Controller {
	public function __construct()
	{	
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/login');
			exit;
		}
	} 
	public function index()
	{
		$data['title'] = 'Data Pengguna';
		$data['pengguna'] = $this->model('PenggunaModel')->getAllPengguna();
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('pengguna/index', $data);
		$this->view('templates/footer');
	}

	public function edit($id){

		$data['title'] = 'Edit Pengguna';
		$data['pengguna'] = $this->model('PenggunaModel')->getPenggunaById($id);
		$data['role'] = $this->model('RoleModel')->getAllRole();
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('pengguna/edit', $data);
		$this->view('templates/footer');
	}

	public function tambah(){
		$data['title'] = 'Tambah Pengguna';	
		$data['role'] = $this->model('RoleModel')->getAllRole();			
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('pengguna/create', $data);
		$this->view('templates/footer');
	}

	public function simpanPengguna(){		
		if($_POST['Password'] == $_POST['ulangi_password']) {	
			$row = $this->model('PenggunaModel')->cekUsername();
			if($row['NamaPengguna'] == $_POST['NamaPengguna']){
				Flasher::setMessage('Gagal','Username yang anda masukan sudah pernah digunakan!','danger');
				header('location: '. base_url . '/pengguna/tambah');
				exit;	
			} else {
				if( $this->model('PenggunaModel')->tambahPengguna($_POST) > 0 ) {
					Flasher::setMessage('Berhasil','ditambahkan','success');
					header('location: '. base_url . '/pengguna');
					exit;			
				} else {
					Flasher::setMessage('Gagal','ditambahkan','danger');
					header('location: '. base_url . '/pengguna');
					exit;	
				}
			}
		} else {
			Flasher::setMessage('Gagal','password tidak sama.','danger');
			header('location: '. base_url . '/pengguna/tambah');
			exit;	
		}
		
	}

	public function updatePengguna(){	
		if(empty($_POST['Password'])) {
			if( $this->model('PenggunaModel')->updateDataPengguna($_POST) > 0 ) {
			Flasher::setMessage('Berhasil','diupdate','success');
			header('location: '. base_url . '/pengguna');
			exit;			
			}else{
				Flasher::setMessage('Gagal','diupdate','danger');
				header('location: '. base_url . '/pengguna');
				exit;	
			}
		} else {
			if($_POST['Password'] == $_POST['ulangi_password']) {
				if( $this->model('PenggunaModel')->updateDataPengguna($_POST) > 0 ) {
				Flasher::setMessage('Berhasil','diupdate','success');
				header('location: '. base_url . '/pengguna');
				exit;			
				}else{
					Flasher::setMessage('Gagal','diupdate','danger');
					header('location: '. base_url . '/pengguna');
					exit;	
				}
			} else {
				Flasher::setMessage('Gagal','password tidak sama.','danger');
				header('location: '. base_url . '/pengguna/tambah');
				exit;	
			}
		}
	}

	public function hapus($id){
		if( $this->model('PenggunaModel')->deletePengguna($id) > 0 ) {
			Flasher::setMessage('Berhasil','dihapus','success');
			header('location: '. base_url . '/pengguna');
			exit;			
		}else{
			Flasher::setMessage('Gagal','dihapus','danger');
			header('location: '. base_url . '/pengguna');
			exit;	
		}
	}
}