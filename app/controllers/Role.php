<?php

class Role extends Controller {
	public function __construct()
	{	
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/login');
			exit;
		}
	} 
	public function index()
	{
		$data['title'] = 'Data Role';
		$data['role'] = $this->model('RoleModel')->getAllRole();
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('role/index', $data);
		$this->view('templates/footer');
	}
	public function cari()
	{
		$data['title'] = 'Data Role';
		$data['role'] = $this->model('RoleModel')->cariRole();
		$data['key'] = $_POST['key'];
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('role/index', $data);
		$this->view('templates/footer');
	}

	public function edit($id)
	{
		$data['title'] = 'Edit Role';
		$data['role'] = $this->model('RoleModel')->getRoleById($id);
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('role/edit', $data);
		$this->view('templates/footer');
	}

	public function tambah() 
	{
		$data['title'] = 'Tambah Role';		
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('role/create', $data);
		$this->view('templates/footer');
	}

	public function simpanRole() 
	{		
		if( $this->model('RoleModel')->tambahRole($_POST) > 0 ) {
			Flasher::setMessage('Berhasil','ditambahkan','success');
			header('location: '. base_url . '/role');
			exit;			
		}else{
			Flasher::setMessage('Gagal','ditambahkan','danger');
			header('location: '. base_url . '/role');
			exit;	
		}
	}

	public function updateRole(){	
		if( $this->model('RoleModel')->updateDataRole($_POST) > 0 ) {
			Flasher::setMessage('Berhasil','diupdate','success');
			header('location: '. base_url . '/role');
			exit;			
		}else{
			Flasher::setMessage('Gagal','diupdate','danger');
			header('location: '. base_url . '/role');
			exit;	
		}
	}

	public function hapus($id){
		if( $this->model('RoleModel')->deleteRole($id) > 0 ) {
			Flasher::setMessage('Berhasil','dihapus','success');
			header('location: '. base_url . '/role');
			exit;			
		}else{
			Flasher::setMessage('Gagal','dihapus','danger');
			header('location: '. base_url . '/role');
			exit;	
		}
	}
}